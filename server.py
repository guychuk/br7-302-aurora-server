from socket import socket
from threading import Thread
from json import load
from messages.MessageFromClient import MessageFromClient
from messages.MessageToClient import MessageToClient
import users_db
import queries_db

# Open the settings.
settings = load(open("settings.json"))

from_clients = []           # This list contains the messages that the server needs to handle.
sockets = set()             # This is a set that contains the sockets that talk to the server.
the_list_is_free = True     # Use this variable to check if the list (from_clients) is free.


def get_messages(sock):
    """
    This function adds the messages from the client to the queue.
    :param sock: The socket between the client and the server.
    :return: Nothing.
    """

    # Use the global variable "the_list_is_free".
    global the_list_is_free

    encoding = settings["communication"]["encoding"]                    # The encoding method.
    message_code_length = settings["messages"]["codes"]["length"]
    bye_message = settings["messages"]["codes"]["fromClient"]["bye"]    # The code for ending the conversation

    while True:
        # Get the code of the next message.
        try:
            code = sock.recv(message_code_length).decode(encoding)
        except Exception as e:
            the_list_is_free = True
            break

        print("GOT MESSAGE:\tsocket = {},\tcode = {}".format(sock.fileno(), code))

        if code == bye_message or code == '':
            break

        # Get the first byte of the message.
        msg = sock.recv(1).decode(encoding)

        # Get the next bytes.
        while msg[-1] != '\n':
            msg += sock.recv(1).decode(encoding)

        # Add the message to the list.

        # Wait until the list is free.
        while not the_list_is_free:
            pass

        # Lock the list, add the message and unlock the list.
        the_list_is_free = False
        from_clients.append(MessageFromClient(code, sock, msg))
        the_list_is_free = True

    # Remove the messages from this client.

    # Wait until the list is free.
    while not the_list_is_free:
        pass

    # Lock the list.
    the_list_is_free = False

    for i in range(len(from_clients)):
        # If the message is from the client, remove the message from the list,
        # delete the message and decrease i.
        if from_clients[i].sock == sock:
            del from_clients[i]
            i -= 1

    # Unlock the list.
    the_list_is_free = True

    print("CLIENT DISCONNECTED:\tsocket = {}".format(sock.fileno()))

    sock.close()

    print("SOCKET CLOSED:\tsocket = {}".format(sock.fileno()))


def answer_messages():
    """
    This function is responsible for building responses to messages (and to respond).
    :return: Nothing.
    """

    # Use the global variable "the_list_is_free".
    global the_list_is_free

    while True:
        # Wait until the list is not empty and free.
        while len(from_clients) == 0 or not the_list_is_free:
            pass

        # Lock the list, get the message and unlock the list.
        the_list_is_free = False
        message = from_clients.pop(0)
        the_list_is_free = True

        # Build a response.
        response = MessageToClient(message)

        # Respond.
        response.send()


def close():
    """
    This function closes the sockets and exits.
    :return: Nothing.
    """

    for sock in sockets:
        sock.send(bytes(settings["messages"]["codes"]["serverBye"], settings["communication"]["encoding"]))
        sock.close()

    exit()


def test():
    input("Continue.")


if __name__ == '__main__':
    # test()

    users_db.create_table()
    queries_db.create_table()

    # Create a socket.
    server_socket = socket()

    # Bind to a port.
    server_socket.bind(('', settings["communication"]["listeningPort"]))

    # Start listening.
    server_socket.listen()
    print("LISTENING")

    last_thread = Thread(target=answer_messages)
    last_thread.start()

    while True:
        # Create a socket.
        client_socket = server_socket.accept()[0]
        sockets.add(client_socket)
        print("CLIENT ACCEPTED:\tsocket = {}".format(client_socket.fileno()))

        # Create a new thread for the client.
        last_thread = Thread(target=get_messages, args=(client_socket,))
        last_thread.start()
