import sqlite3


def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("users",) in answer


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print("CREATING TABLE:\tname = users,\tfile = users.db")
        connection = sqlite3.connect("users.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE users(username STRING, password STRING)")
        connection.commit()
        connection.close()

    return None


def user_exists(username):
    """
    Checks if a user exists in the table.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def insert_user(username, password):
    """
    This function inserts a new user to the table.
    :param username: The user's username.
    :param password: The user's password.
    :return: Trye if inserted, False if not.
    """

    # Username and password can't be empty
    if len(username) == 0 or len(password) == 0:
        return False

    # If the user exists, then this function does nothing
    if user_exists(username):
        return False

    result = bool()

    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    print("INSERTING USER:\tusername = {},\tpassword = {}".format(username, password))
    cursor.execute("INSERT INTO users(username, password) VALUES(?, ?)", (username, password))
    connection.commit()

    connection.close()

    result = user_exists(username)

    if result:
        print("USER INSERTED:\tusername = {},\tpassword = {}".format(username, password))
    else:
        print("FAILED TO INSERT USER:\tusername = {},\tpassword = {}".format(username, password))

    return result


def is_correct(username, password):
    """
    Check if the password matches the username.
    :param username: A username.
    :param password: A password.
    :return: True if match, False if not.
    """

    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", [username, password])
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("users.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM users")
    answer = cursor.fetchall()

    connection.close()

    print("Users table:")

    for user in answer:
        print(user[0], user[1])

    print()

    return None
