from json import load

# Open the settings.
settings = load(open("settings.json"))

# Codes for messages from the client.
codes_from_client = settings["messages"]["codes"]["fromClient"]


class MessageFromClient:
    """ This class represents a message from the client. """

    def __init__(self, code, sock, data):
        """
        This is the class' constructor.
        :param sock: The socket between the server and the client.
        :param data: The message before its processing.
        """

        self.data = data[3:-1]  # The message
        self.sock = sock        # The socket between the client and the server
        self.code = code        # The message's code

        self.process()

    def process(self):
        """
        This function processes the message.
        The function creates the relevant variables for the message,
        based on its code.
        :return: None.
        """

        division = settings["messages"]["division"]

        message_parts = self.data.split(division["main"])

        self.username = message_parts[0]

        if codes_from_client["googleQuery"] == self.code:
            self.search_query = message_parts[1]
        elif codes_from_client["weatherQuery"] == self.code:
            self.city_name = message_parts[1]
            self.country_name = message_parts[2]
            self.units = message_parts[3]
        elif codes_from_client["loginOrRegister"] == self.code:
            self.password = message_parts[1]
        elif codes_from_client["getHistory"] == self.code:
            pass
        elif codes_from_client["calculate"] == self.code:
            self.calculation = message_parts[1]
