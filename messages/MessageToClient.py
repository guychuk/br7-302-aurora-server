from json import load
import users_db
import queries_db

# Open the settings.
settings = load(open("settings.json"))

# Codes for messages to the client.
codes_to_client = settings["messages"]["codes"]["toClient"]


class MessageToClient:
    """ This class represents a response to the client. """

    def __init__(self, message_from_client):
        self.sock = message_from_client.sock            # The socket between the server and the client.
        self.msg_from_client = message_from_client      # The message from the client.
        self.message_content = str()                    # The content of THIS message, the response.
        self.code = str()                               # This message's code.
        self.username = message_from_client.username    # The user's username

        self.build_message()

    def build_message(self):
        """
        Build a response for the message from the client.
        The content of the response will be stored in self.message_content.
        :return: None.
        """

        codes_from_client = settings["messages"]["codes"]["fromClient"]     # Codes for messages from the client.
        msg_from_client_code = self.msg_from_client.code                    # The code of the message from the client.

        if codes_from_client["googleQuery"] == msg_from_client_code:
            self.handle_google_search()
        elif codes_from_client["weatherQuery"] == msg_from_client_code:
            self.handle_weather_request()
        elif codes_from_client["loginOrRegister"] == msg_from_client_code:
            self.handle_login_or_register()
        elif codes_from_client["getHistory"] == msg_from_client_code:
            self.handle_history()

    def handle_google_search(self):
        """
        Handle a google search message.
        Do the search, get the relevant data and build a message.
        :return: None.
        """

        from internet import google

        # Get the code of the response
        self.code = codes_to_client["googleResult"]

        # Get the query and the username
        query = self.msg_from_client.search_query
        username = self.username

        # Insert the query to the db
        queries_db.insert_user(username, "Googled " + query)

        print("HANDLING MESSAGE:"
            + "\n\tusername = " + username
            + "\n\tcode = " + self.msg_from_client.code
            + "\n\tquery = " + query)

        # Get the relevant data from Google.
        data = google.get_relevant_data(query, title=True, link=True, snippet=True)

        # Build the message
        main_division = settings["messages"]["division"]["main"]            # The division between results.
        secondary_division = settings["messages"]["division"]["secondary"]  # The division between parts in a result.

        for result in data:
            for field in result:
                self.message_content += result[field] + secondary_division

            # Remove the last secondary division.
            self.message_content = self.message_content[:-len(secondary_division)]

            self.message_content += main_division

        # Remove the last main division and the new-lines chars.
        self.message_content = self.message_content[:-len(main_division)].replace('\n', '')

    def handle_weather_request(self):
        """
        Handle a weather query.
        Do the search, get the relevant data and build a message.
        :return: None.
        """

        from internet import openweathermap

        city = self.msg_from_client.city_name[1:-1]
        country = self.msg_from_client.country_name[1:-1]
        units = self.msg_from_client.units

        full_query = "Weather in " + city;
        username = self.username

        city = city.replace("'", '')

        if (country != ""):
            full_query += " in {}".format(country)

        if (units != ""):
            full_query += " ({})".format(units)

        queries_db.insert_user(username, full_query)

        code = self.msg_from_client.code

        print(("HANDLING MESSAGE:"
             + "\n\tusername = " + username
             + "\n\tcode = " + code
             + "\n\tcity = " + city
             + "\n\tcountry = " + country
             + "\n\tunits = " + units))

        # Get the relevant data from Google.
        if country != '':
            data = openweathermap.get_relevant_data(city, country, units,
                                                    temp=True, weather_main=True, weather_description=True)
        else:
            data = openweathermap.get_relevant_data(city, None, units,
                                                    temp=True, weather_main=True, weather_description=True)

        # The division between fields.
        main_division = settings["messages"]["division"]["main"]

        for field in data:
            self.message_content += str(data[field]) + main_division

        if (len(data) == 0):
            self.code = codes_to_client["weatherFailed"]
        else:
            self.message_content = self.message_content[:-len(main_division)]
            self.code = codes_to_client["weather"]

    def handle_login_or_register(self):
        username = self.msg_from_client.username
        password = self.msg_from_client.password

        print("HANDLING MESSAGE:"
             + "\n\tcode = " + self.msg_from_client.code
             + "\n\tusername = " + username
             + "\n\tpassword = " + password)

        if users_db.user_exists(username):
            if users_db.is_correct(username, password):
                self.code = codes_to_client["loginOK"]      # User exists and correct password
            else:
                self.code = codes_to_client["loginFail"]    # User exists but incorrect password
        elif users_db.insert_user(username, password):
            self.code = codes_to_client["loginOK"]          # User created
        else:
            self.code = codes_to_client["loginFail"]        # Failed to create a new user

    def handle_history(self):
        self.code = codes_to_client["sendHistory"]

        username = self.msg_from_client.username

        print("HANDLING MESSAGE:"
             + "\n\tusername = " + username
             + "\n\tcode = " + self.msg_from_client.code)

        list_of_queries = queries_db.get_queries(username)

        for query in list_of_queries:
            self.message_content += "###" + query

        if (self.message_content != 0):
            self.message_content = self.message_content[3:]

    def handle_calculation(self):
        self.code = codes_to_client["calculation"]

        calculation = self.msg_from_client.calculation
        username = self.msg_from_client.username

        print("HANDLING MESSAGE:"
              + "\n\tcode = " + self.msg_from_client.code
              + "\n\tusername = " + username
              + "\n\tcalculation = " + calculation)

        self.message_content = str(exec(calculation))

    def send(self):
        """
        Send the message to the client.
        :return: None.
        """

        if len(self.message_content) == 0:
            message = self.code + '\n'
        else:
            message = self.code + "###" + self.message_content + '\n'

        print("SENDING MESSAGE:"
             + "\n\tusername = " + self.msg_from_client.username
             + "\n\tcontent = " + message[:-1])
        self.sock.send(bytes(message, settings["communication"]["encoding"]))
