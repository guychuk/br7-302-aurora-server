import urllib.request
from json import load


# Open the settings.
settings = load(open("settings.json"))


def search(query):
    """
    Use this function to search in google.
    :param query: What to search?
    :return: A dictionary with search results.
    """

    key = settings["google"]["key"]     # The project's key (from Google).
    cx = settings["google"]["cx"]       # The search engine.
    n = settings["google"]["searches"]  # The number of results to get from Google.

    # A space will raise "HTTPError 400: 'Bad Request'"
    if ' ' in query:
        q = query.replace(' ', settings["urls"]["space"])
    else:
        q = query

    url = "https://www.googleapis.com/customsearch/v1?key={}&cx={}&q={}&num={}".format(key, cx, q, n)

    # Get the results.
    results = urllib.request.urlopen(url)

    # Return the results in a dictionary.
    return load(results)


def get_relevant_data(query,
                      kind=False, title=False, html_title=False, link=False,
                      display_link=False, snippet=False, html_snippet=False,
                      cache_id=False, formatted_url=False, html_formatted_url=False):
    """
    Extract the relevant data from the results.
    :param query: What to search?
    :param kind: Is "kind" relevant?
    :param title: Is "title" relevant?
    :param html_title: Is "htmlTitle" relevant?
    :param link: Is "link" relevant?
    :param display_link: Is "displayLink" relevant?
    :param snippet: Is "snippet" relevant?
    :param html_snippet: Is "htmlSnippet" relevant?
    :param cache_id: Is "cacheId" relevant?
    :param formatted_url: Is "formattedUrl" relevant?
    :param html_formatted_url: Is "htmlFormattedUrl" relevant?
    :return: A list of results with the relevant data about them.
    """

    # Get the results from Google.
    search_results = search(query)

    # If there are no results, return an empty list.
    if search_results["queries"]["request"][0]["totalResults"] == "0":
        return []

    # This variable will store the relevant data.
    relevant_data = list()

    items = search_results["items"]

    for item in items:
        # Build a new dictionary for this result.
        result = dict()

        if kind:
            result["kind"] = item["kind"]

        if title:
            result["title"] = item["title"]

        if html_title:
            result["htmlTitle"] = item["htmlTitle"]

        if link:
            result["link"] = item["link"]

        if display_link:
            result["displayLink"] = item["displayLink"]

        if snippet:
            result["snippet"] = item["snippet"]

        if html_snippet:
            result["htmlSnippet"] = item["htmlSnippet"]

        if cache_id:
            result["cacheId"] = item["cacheId"]

        if formatted_url:
            result["formattedUrl"] = item["formattedUrl"]

        if html_formatted_url:
            result["htmlFormattedUrl"] = item["htmlFormattedUrl"]

        relevant_data.append(result)

    return relevant_data
