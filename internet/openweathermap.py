import urllib.request
from json import load


# Open the settings.
settings = load(open("settings.json"))


def get_weather(city, country=None, units=None):
    """
        Use this function to get the current weather for a city.
        This function gets the weather from OpenWeatherMap.
        :param city: The city.
        :param country: The country where the city is located.
        :param units: Fahrenheit ("imperial"), Celsius ("metric") or Kelvin (default).
        :return: A dictionary than contains the data from OpenWeatherMap.
        :raises: ValueError if the units are invalid.
    """

    api_key = settings["openWeatherMap"]["APIKey"]
    server_name = settings["openWeatherMap"]["serverName"]
    optional_units = settings["openWeatherMap"]["optionalUnits"]

    call = "https://{}/data/2.5/weather?appid={}".format(server_name, api_key)

    if units is not None and units not in optional_units:
        raise ValueError("invalid units ({}).".format(units))
    elif city.replace(' ', '').replace('    ', '') == '':
        raise ValueError("empty city name.")

    call += "&units={}".format(units)

    # A space will raise "HTTPError 400: 'Bad Request'"
    if ' ' in city:
        city_name = city.replace(' ', settings["urls"]["space"])
    else:
        city_name = city

    if country is not None:
        # A space will raise "HTTPError 400: 'Bad Request'"
        if ' ' in country:
            country_name = country.replace(' ', settings["urls"]["space"])
        else:
            country_name = country

        location = "{},{}".format(city_name, country_name)
    else:
        location = city_name

    call += "&q={}".format(location)

    # TODO: HANDLE HTTP ERROR 404
    try:
        results = urllib.request.urlopen(call)
    except Exception as e:
        return []

    # Return the results in a dictionary.
    return load(results)


def get_relevant_data(city, country=None, units=None,
                      temp=False, pressure=False, humidity=False,
                      weather_main=False, weather_description=False):
    """
    Extract the relevant data from the results.
    :param city: The city.
    :param country: The country where the city is located.
    :param units: Fahrenheit ("imperial"), Celsius ("metric") or Kelvin (default)
    :param temp: The temperature.
    :param pressure: Pressure.
    :param humidity: Humidity (%).
    :param weather_main: Weather in a word.
    :param weather_description: Description of the weather in a sentence.
    :return: The relevant data.
    """

    # Get the results from Google.
    weather = get_weather(city, country, units)

    if (len(weather) == 0):
        return []

    # This variable will store the relevant data.
    relevant_data = dict()

    if temp:
        relevant_data["temperature"] = weather["main"]["temp"]

    if pressure:
        relevant_data["pressure"] = weather["main"]["pressure"]

    if humidity:
        relevant_data["humidity"] = weather["main"]["humidity"]

    if weather_main:
        relevant_data["weather"] = weather["weather"][0]["main"]

    if weather_description:
        relevant_data["weatherDescription"] = weather["weather"][0]["description"]

    return relevant_data


def results_string(city, country=None, units=None):
    """
    Get a string of results.
    :param city: The city.
    :param country: The country where the city is located.
    :param units: Fahrenheit ("imperial"), Celsius ("metric") or Kelvin (default)
    :return: A string of results, separated.
    """

    # Get the relevant data from Google.
    data = get_relevant_data(city, country, units, temp=True, weather_main=True, weather_description=True)

    if (len(data) == 0):
        return ""

    # The division between fields.
    main_division = settings["messages"]["division"]["main"]

    # The result, the return value of this function.
    string = str()

    for field in data:
        string += str(data[field]) + main_division

    return string[:-len(main_division)]
