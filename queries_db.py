import sqlite3


def table_exists():
    """
    Checks if a table exists in the db.
    :return: True if exists, False if not.
    """

    connection = sqlite3.connect("queries.db")
    cursor = connection.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    answer = cursor.fetchall()

    connection.close()

    return ("queries",) in answer


def create_table():
    """
    Creates the table in the db.
    :return: None.
    """

    if not table_exists():
        print("CREATING TABLE:\tname = queries,\tfile = queries.db")
        connection = sqlite3.connect("queries.db")
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE queries(username STRING, query STRING)")
        connection.commit()
        connection.close()

    return None


def get_queries(username):
    """
    Get the wueries that the username sent.
    :param username: The username of the user.
    :return: A list of queries.
    """

    connection = sqlite3.connect("queries.db")
    cursor = connection.cursor()

    cursor.execute("SELECT query FROM queries WHERE username = ?", (username,))
    answer = cursor.fetchall()

    connection.close()

    result = []
    
    for ans in answer:
        result.append(ans[0])

    return result


def insert_user(username, query):
    """
    This function inserts a new user to the table.
    :param username: The user's username.
    :param query: The query.
    :return: True if inserted, False if not.
    """

    # Username and query can't be empty
    if len(username) == 0 or len(query) == 0:
        return False

    connection = sqlite3.connect("queries.db")
    cursor = connection.cursor()

    print("INSERTING QUERY:\tusername = {},\tquery = {}".format(username, query))
    cursor.execute("INSERT INTO queries(username, query) VALUES(?, ?)", (username, query))
    connection.commit()

    connection.close()

    result = query_exists(username, query)

    if result:
        print("QUERY INSERTED:\tusername = {},\tquery = {}".format(username, query))
    else:
        print("FAILED TO INSERT QUERY:\tusername = {},\tquery = {}".format(username, qurty))

    return result


def print_table():
    """
    This function prints the table.
    :return: None.
    """

    connection = sqlite3.connect("queries.db")
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM queries")
    answer = cursor.fetchall()

    connection.close()

    print("Queries table:")

    for query in answer:
        print(query[0], query[1])

    return None


def query_exists(username, query):
    """
    Checks if a user query in the table.
    :param username: The username of the user.
    :return: True of exists, False if not.
    """

    connection = sqlite3.connect("queries.db")
    cursor = connection.cursor()

    cursor.execute("SELECT query FROM queries WHERE username = ? AND query = ?", [username, query])
    answer = cursor.fetchall()

    connection.close()

    return bool(len(answer))